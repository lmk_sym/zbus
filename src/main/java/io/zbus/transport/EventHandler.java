package io.zbus.transport;

public interface EventHandler { 
	void handle() throws Exception;   
}